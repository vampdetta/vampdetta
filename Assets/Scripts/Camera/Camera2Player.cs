﻿using UnityEngine;
using System.Collections;

public class Camera2Player : MonoBehaviour {
	GameObject Player1;
	GameObject Player2;
	GameObject[] Players;
	float yOffset = 20f;
	float zOffset = 15f;
	Camera cam;
	float Distance;
	public float MAX_DISTANCE = 20f;
	public float PULL_DISTANCE = 10f;
	public float MIN_DISTANCE = 5f;
	float timer;
	bool pullDistance;
	public float pullTimer = 5f;

	// Use this for initialization
	void Start () {
		Distance = 0f;
		timer = 0f;
		cam = GetComponent<Camera> ();
		Players = GameObject.FindGameObjectsWithTag ("Player");
		if (Players [0].name == "Player1") {
			Player1 = Players [0];
			Player2 = Players [1];
		} else {
			Player1 = Players [1];
			Player2 = Players [0];
		}
	}

	float ClampDistance(float dis){
		if (dis > MAX_DISTANCE) {
			dis = MAX_DISTANCE;
			Debug.Log ("MAX distance");
		} 
		if (dis < MIN_DISTANCE) {
			dis = MIN_DISTANCE;
			Debug.Log ("min distance");
		}
		return dis;
	}

	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		Vector3 temp = (Player1.transform.position + Player2.transform.position)/2;
		temp.y += yOffset;
		temp.z -= zOffset;
		transform.position = temp;
		Distance = Vector3.Distance (Player1.transform.position, Player2.transform.position);
		Debug.Log ("BEFore = " + Distance);
		Distance = ClampDistance (Distance);
//		ClampDistance ();
		Debug.Log ("Distance = " + Distance);
		Debug.Log ("FOV = " + cam.fieldOfView);
		cam.fieldOfView = Distance * 2;
	}
}