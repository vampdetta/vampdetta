﻿using UnityEngine;
using System.Collections;

public class ParticleDeath : MonoBehaviour
{
    private ParticleSystem particleSystem;
	private AudioSource sound;
	public float timer = 0f;
	public float DeathTime = 3f;
	public bool followPlayer;

	// Use this for initialization
	void Start ()
    {
		sound = GetComponent<AudioSource> ();
        particleSystem = GetComponent<ParticleSystem>();
		if (sound != null) {
			sound.Play ();
		}
		DeathTime += Time.deltaTime;
	}

	public void DestroyObject(){
		Destroy (this.gameObject);
	}

	// Update is called once per frame
	void Update ()
	{
		if (followPlayer) {
			Vector3 pos = PlayerMovement.playerPosition.position;
			pos.x += PlayerInput.H;
			pos.z += PlayerInput.V;
			gameObject.transform.position = pos;
		}
		timer += Time.deltaTime;
		if (timer > DeathTime || particleSystem.isStopped) {
			Destroy(this.gameObject);
		}
	}
}