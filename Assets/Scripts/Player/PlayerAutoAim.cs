﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAutoAim : MonoBehaviour {
	public GameObject TargetCurrent;
	public bool LockedOn;
	List<GameObject> TargetPossibles;
	private Color original;
	private Color PlayerColor;
	private int index;
	private float rotateStregth;
	public GameObject Player;
	Transform circle1;
	Transform circle2;
	Transform circle3;
	float rotateSpeed = 5.0f;
	KeyCode left_target;
	KeyCode right_target;
	PlayerHealth playerHealth;

	// Use this for initialization
	void Start () {
		LockedOn = false;
		TargetCurrent = null;
		index=0;
		TargetPossibles = new List<GameObject> ();
		PlayerColor = this.gameObject.GetComponentInParent<Light> ().color;
		playerHealth = GetComponentInParent<PlayerHealth> ();
		if (playerHealth.gameObject.name == "Player1") {
			left_target = PlayerInput.LEFT_TARGET;
			right_target = PlayerInput.RIGHT_TARGET;
		} else if (playerHealth.gameObject.name == "Player2") {
			left_target = PlayerInput.LEFT_TARGET2;
			right_target = PlayerInput.RIGHT_TARGET2;
		}
	}

	void OnTriggerEnter(Collider other){
		EnemyHealth temp = other.gameObject.GetComponentInParent<EnemyHealth> ();
		if (temp != null) {
			if (temp.gameObject.tag == "Enemy" && other.GetType () == typeof(CapsuleCollider)) {
				TargetPossibles.Add (other.gameObject);
			}
		}
	}

	void OnTriggerExit(Collider other){
		EnemyHealth temp = other.gameObject.GetComponentInParent<EnemyHealth> ();
		if (temp != null) {
			if (temp.gameObject.tag == "Enemy" && other.GetType () == typeof(CapsuleCollider)) {
				removeTarget (other.gameObject);
			}
		}
	}

	public void removeTarget(GameObject other){
		if (other == TargetCurrent) {
			other.GetComponent<Light> ().color = original;
			//TargetPossibles.Remove (other.gameObject);
			if (circle1 != null) {
				circle1.gameObject.SetActive (false);
			}
			circle1 = null;
			LockedOn = false;
		}TargetPossibles.Remove (other.gameObject);
	}

	void setTarget(GameObject target){
		if (TargetCurrent != null) {
			TargetCurrent.gameObject.GetComponent<Light> ().color = original;
		}
		if (circle1 != null) {
			circle1.gameObject.SetActive (false);
			circle1 = null;
		}
		TargetCurrent = target;
		original = TargetCurrent.gameObject.GetComponent<Light> ().color;
		TargetCurrent.gameObject.GetComponent<Light> ().color = PlayerColor;
		if (playerHealth.gameObject.name == "Player1") {
			circle1 = target.gameObject.transform.Find ("circle1");
		}
		else if(playerHealth.gameObject.name == "Player2"){
			circle1 = target.gameObject.transform.Find ("circle2");
		}
		circle1.gameObject.SetActive (true);
		LockedOn = true;
	}

	int updateIndex(int num, bool left){
		if (left) {
			num -= 1;
		} else {
			num += 1;
		}

		if (num < 0) {
			num = TargetPossibles.Count - 1;
		}else if(num >= TargetPossibles.Count){
			num = 0;
		}
		return num;
	}

	// Update is called once per frame
	void Update () {
		if (!LockedOn) { 
			if (TargetPossibles.Count != 0) {
				index = 0;
				setTarget (TargetPossibles [index]);
			}
		} else { // Loacked on to target
			if (circle1 != null) {
				circle1.Rotate (0, 0, rotateSpeed);
			}
		}

		if (TargetPossibles.Count == 0) {
			LockedOn = false;
		}
			
		if (Input.GetKeyDown( left_target )) {
			if (TargetPossibles.Count > 1) {
				index = updateIndex (index, true);
				setTarget (TargetPossibles [index]);
			}
		}

		if (Input.GetKeyDown( right_target )) {
			if (TargetPossibles.Count > 1) {
				index = updateIndex (index, false);
				setTarget (TargetPossibles [index]);
			}
		}
	}
}
