﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
	public static float spawnTime = 3f;
	public static int current_Spawned = 0;

	public float SPAWN_DELAY = 5f;
	public int MAX_AT_TIME = 20;
	public int TOTAL_PER_LEVEL = 50;

	private int total_Spawned = 0;
	private float time = 0f; 
	private float till_spawn = 0f;

    public GameObject[] enemyPrefabs;
    public Transform[] spawnPoints;
	GameObject[] players;

    void Start ()
    {
		total_Spawned = 0;
		time = 0f;
		spawnTime = SPAWN_DELAY;
		till_spawn = time + SPAWN_DELAY;
		players = GameObject.FindGameObjectsWithTag ("Player");
    }

	void Update(){
		time += Time.deltaTime;
		SPAWN_DELAY = spawnTime;
		if (time > till_spawn && current_Spawned < MAX_AT_TIME && total_Spawned < TOTAL_PER_LEVEL) {
			Spawn ();
			till_spawn = time + SPAWN_DELAY;
		}
	}
		
    void Spawn ()
	{
		bool end = false;
		for (int i = 0; i < players.Length; i++) {
			if (players [i].GetComponent<PlayerHealth> ().isDead) {
				end = true;
			} else {
				end = false;
			}
		}
		if(end){
            return;
        }

		total_Spawned += 1;
		current_Spawned += 1;

		int enemyIndex = Random.Range (0, enemyPrefabs.Length);
        int spawnPointIndex = Random.Range (0, spawnPoints.Length);

		Instantiate (enemyPrefabs[enemyIndex], spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }

	public static void EnemyDeath(){
		current_Spawned -= 1;
	}
}
