﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;

	PlayerHealth playerHealth1;
    Animator anim;
    EnemyHealth enemyHealth;
	EnemyMovement enemyMovement;
    public bool playerInRange;
    float timer;
	bool isAttacking;

    void Awake ()
    {
        enemyHealth = GetComponent<EnemyHealth>();
		enemyMovement = GetComponent<EnemyMovement>();
		anim = GetComponentInChildren<Animator> ();
    }

    void OnTriggerEnter (Collider other)
    {
		PlayerHealth temp = other.gameObject.GetComponentInParent<PlayerHealth> ();
		if (temp != null) {
			if (!temp.isDead && temp.gameObject == enemyMovement.target && other.GetType () == typeof(CapsuleCollider)) {
				playerInRange = true;
			}
		}
    }
		
    void OnTriggerExit (Collider other)
	{	
		PlayerHealth temp = other.gameObject.GetComponentInParent<PlayerHealth> ();
		if (temp != null) {
			if (temp.gameObject == enemyMovement.target && other.GetType () == typeof(CapsuleCollider)) {
				playerInRange = false;
			}
		}
    }
		
    void Update ()
    {
		if (enemyMovement.target != null) {
			if (enemyMovement.target.GetComponent<PlayerHealth> ().isDead) {
				playerInRange = false;
			}
		}
		if (enemyMovement.target == null) {
			isAttacking = true;
			playerInRange = false;
		}
        timer += Time.deltaTime;
		isAttacking = false;
	
		if(timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0){
            Attack ();
        }

		anim.SetBool ("isAttacking", isAttacking);

		//if(PlayerHealth.isDead){
        //    anim.SetTrigger ("PlayerDead");
        //}
    }
		
    void Attack ()
    {
        timer = 0f;
		isAttacking = true;
		if (enemyMovement.target != null) {
			enemyMovement.target.GetComponent<PlayerHealth> ().TakeDamage (attackDamage);
			if (enemyMovement.target.GetComponent<PlayerHealth> ().isDead) {
				playerInRange = false;
			}
		}

    }
}
