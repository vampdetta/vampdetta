﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
	public GameObject player1;
	public GameObject player2;
	GameObject[] players;
	EnemyHealth enemyHealth;
	NavMeshAgent nav;
	Animator anim;
	public bool isWalk;
	public GameObject target;

    void Awake ()
    {
        players = GameObject.FindGameObjectsWithTag ("Player");
		for (int i = 0; i < players.Length; i++) {
			if (players [i].name == "Player1") {
				player1 = players [i];
			}
			if (players [i].name == "Player2") {
				player2 = players [i];
			}
		}
		target = null;
		anim = GetComponentInChildren<Animator> ();
        enemyHealth = GetComponent <EnemyHealth> ();
        nav = GetComponent <NavMeshAgent> ();
    }

	void setTarget(){
		float distance1 = Vector3.Distance (player1.transform.position, this.transform.position);
		float distance2 = Vector3.Distance (player2.transform.position, this.transform.position);
		if (distance1 <= distance2 && !player1.GetComponent<PlayerHealth> ().isDead) {
			target = player1;
		}else if(!player1.GetComponent<PlayerHealth> ().isDead && player2.GetComponent<PlayerHealth> ().isDead){
			target = player1;
		}else if (player1.GetComponent<PlayerHealth> ().isDead && player2.GetComponent<PlayerHealth> ().isDead) {
			target = null;
			GameOverManager.GameOver = true;
		} else {
			target = player2;
		}
	}

    void Update (){
		setTarget ();
		if (enemyHealth.currentHealth > 0 && target!= null) {
			nav.SetDestination (target.transform.position);
			isWalk = true;
		} 
		else {
			nav.enabled = false;
			isWalk = false;
		}
		anim.SetBool ("isWalking", isWalk);

    }
}
